# nodes = {}#{n1:{nei1:ls1,nei2:ls2,nei3:ls3},n2:{nei1:ls1,...}}
'''
KUROSE and ROSS COMPUTER NETWORKING

# initialisation:
visited = {u}
for all nodes v
if v is a neighbour of u:
    then D[v] = c(u,v)
else D(v) = inf

# Loop
find w not in visited such that D(w) is a minimum 
add w to visited
update D(v) for each neighbour v of w and not in visited:
    D(v) = min(D(v),D(w) + c(w,v))
until visited == graph

'''
def sort_dist(dist):
    dist_list=[]
    for k in sorted(dist,key=dist.get):
        dist_list.append([k,dist[k]])
    return dist_list

def frame_path(parent,src,dst):
    path=[dst]
    while dst != src:
        p = parent[dst]
        path.append(p)
        dst=p
    path.reverse()
    return path

def shortestpath(G,src,dst):
    print("in spf module")
    visited = [src]
    dist = {}
    parents = {src:src} # update this to track last good node using that lead us here
    for node in G.keys():
        if node in G[src].keys():
            dist[node]=G[src][node]
            parents[node]=src
        else:
            dist[node] = 1000000 # some random high value
    dist[src]=0
    dl = sort_dist(dist) # this will arrange the dictionary from lowest to highest values
    while len(visited) < len(G):
        node = dl.pop(0) # lowest cost node
        if node[0] not in visited:
            visited.append(node[0])
            for nei in G[node[0]].keys():
                if nei not in visited:
                    dist[nei] = min(dist[nei] , dist[node[0]]+G[node[0]][nei])
                    if dist[nei] == dist[node[0]]+G[node[0]][nei]:
                        parents[nei] = node[0]
                    dl = sort_dist(dist)
    path = frame_path(parents,src,dst)
    #print(path)
    return path 
'''
G = {'a':{'b':2,'c':4,'d':7},'b':{'a':2,'c':3,'d':4},'c':{'d':2,'b':3,'a':4},'d':{'c':2,'a':7,'b':4}}
paths = shortestpath(G,'a','d')
print (paths)
'''
            
            
